-----------------------------------------------------------------------------------------------
-- Client Lua Script for DeBuff_Rave
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
 
-----------------------------------------------------------------------------------------------
-- DeBuff_Rave Module Definition
-----------------------------------------------------------------------------------------------
local DeBuff_Rave = {} 
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
-- e.g. local kiExampleVariableMax = 999
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function DeBuff_Rave:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 

    -- initialize variables here

    return o
end

function DeBuff_Rave:Init()
	local bHasConfigureFunction = false
	local strConfigureButtonText = ""
	local tDependencies = {
		-- "UnitOrPackageName",
	}
    Apollo.RegisterAddon(self, bHasConfigureFunction, strConfigureButtonText, tDependencies)
end
 

-----------------------------------------------------------------------------------------------
-- DeBuff_Rave OnLoad
-----------------------------------------------------------------------------------------------
function DeBuff_Rave:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("DeBuff_Rave.xml")
	self.xmlDoc:RegisterCallback("OnDocLoaded", self)
end

-----------------------------------------------------------------------------------------------
-- DeBuff_Rave OnDocLoaded
-----------------------------------------------------------------------------------------------
function DeBuff_Rave:OnDocLoaded()

	if self.xmlDoc ~= nil and self.xmlDoc:IsLoaded() then
	    self.wndMain = Apollo.LoadForm(self.xmlDoc, "DeBuff_RaveForm", nil, self)
		if self.wndMain == nil then
			Apollo.AddAddonErrorText(self, "Could not load the main window for some reason.")
			return
		end
		
	    self.wndMain:Show(false, true)

		-- if the xmlDoc is no longer needed, you should set it to nil
		-- self.xmlDoc = nil
		
		-- Register handlers for events, slash commands and timer, etc.
		-- e.g. Apollo.RegisterEventHandler("KeyDown", "OnKeyDown", self)
		Apollo.RegisterSlashCommand("debuffer", "OnDeBuff_RaveOn", self)


		-- Do additional Addon initialization here
	end
end

-----------------------------------------------------------------------------------------------
-- DeBuff_Rave Functions
-----------------------------------------------------------------------------------------------
-- Define general functions here

-- on SlashCommand "/debuffer"
function DeBuff_Rave:OnDeBuff_RaveOn()
	self.wndMain:Invoke() -- show the window
end


-----------------------------------------------------------------------------------------------
-- DeBuff_RaveForm Functions
-----------------------------------------------------------------------------------------------
-- when the OK button is clicked
function DeBuff_Rave:OnOK()
	self.wndMain:Close() -- hide the window
end

-- when the Cancel button is clicked
function DeBuff_Rave:OnCancel()
	self.wndMain:Close() -- hide the window
end


-----------------------------------------------------------------------------------------------
-- DeBuff_Rave Instance
-----------------------------------------------------------------------------------------------
local DeBuff_RaveInst = DeBuff_Rave:new()
DeBuff_RaveInst:Init()
